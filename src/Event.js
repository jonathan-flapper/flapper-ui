import {Card, CardBody, CardFooter, CardHeader, CardSubtitle, CardText} from "reactstrap";

function Event({event}) {
  return (
    <Card xs={3}>
      <CardHeader align="left" tag="h4">{event.name}</CardHeader>
      <CardBody>
        <CardText align="left">Location: {event.location}</CardText>
        <CardText align="left">{event.description || "No description"}</CardText>
        <CardText align="left">Event Date: {event.date}</CardText>
      </CardBody>
      <CardFooter align="left">{event.category}</CardFooter>
    </Card>
  )
}

export default Event;
