import './App.css';
import Events from "./Events";
import EventSearch from "./EventSearch";
import {Container} from "reactstrap";

function App() {
  return (
    <div className="App">
      <Container>
        <EventSearch/>
        <Events/>
      </Container>
    </div>
  );
}

export default App;
