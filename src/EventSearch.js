import {Button, Col, Container, FormGroup, Input, Row} from "reactstrap";

function EventSearch() {
  return (
    <FormGroup>
      <Container>
        <Row>
          <Col><Input type="search" name="topics" id="topics" placeholder="event topics"/></Col>
          <Col><Input type="date" name="date" id="date" placeholder="event date"/></Col>
          <Col><Button color="primary" onClick>Search</Button></Col>
        </Row>
      </Container>
    </FormGroup>
  );
}

export default EventSearch;
