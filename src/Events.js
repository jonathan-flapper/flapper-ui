import {useEffect, useState} from "react";
import Event from "./Event";
import {CardColumns, Container} from "reactstrap";

function Events() {
  const [events, setEvents] = useState([]);

  useEffect(
    () => {
      fetch(
        'http://localhost:8080/events/q',
        {
          method: 'GET',
          headers: new Headers({
            Accept: "application/json"
          })
        }
      )
        .then(it => it.json())
        .then(it => {
          setEvents(it)
        })
        .catch(it => console.log(it))
    },
    []
  );

  return (
    <Container>
      <CardColumns>
        {events.map(e => <Event key={e.id} event={e}/>)}
      </CardColumns>
    </Container>
  )
}

export default Events;
